for %%A in (assets, 
	controlmybot, 
	docs, docs.wiki, meta, 
	sentry-discord, 
	TwitchPlays, 
	web, 
	websocket-foundation) DO (
	cd %%A
	git fetch --all
	cd ..
)
PAUSE
